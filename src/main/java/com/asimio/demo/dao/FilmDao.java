package com.asimio.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asimio.demo.domain.Film;

@Repository
public interface FilmDao extends JpaRepository<Film, Integer> {

    // Uncomment to fix N+1 Select problem - https://tech.asimio.net/2020/11/06/Preventing-N-plus-1-select-problem-using-Spring-Data-JPA-EntityGraph.html
    // Uncomment causes another problem:
    // QueryTranslatorImpl   : HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!
    // Find HHH000104 fix at https://tech.asimio.net/2021/05/19/Fixing-Hibernate-HHH000104-firstResult-maxResults-warning-using-Spring-Data-JPA.html

//    @EntityGraph(
//            type = EntityGraphType.FETCH,
//            attributePaths = {
//                    "language",
//                    "filmActors", "filmActors.actor"
//            }
//    )
//    Page<Film> findAll(Pageable pageable);
//
//    @EntityGraph(
//            type = EntityGraphType.FETCH,
//            attributePaths = {
//                    "language",
//                    "filmActors", "filmActors.actor"
//            }
//    )
//    Optional<Film> findById(Integer id);
}